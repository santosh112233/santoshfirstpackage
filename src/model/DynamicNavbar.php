<?php
namespace Santosh\Mypackage\Model;

use Illuminate\Database\Eloquent\Model;

class DynamicNavbar extends Model
{
    protected $table = 'dynamic_navbars';
    protected $guarded = [];
    public function childerns()
    {
    	return $this->hasMany('Santosh\Mypackage\Model\DynamicNavbar', 'parent_id');
    }

    public function parent() {
    return $this->belongsTo('Santosh\Mypackage\Model\DynamicNavbar', 'parent_id'); 
}

	public function getRouteKeyName() {
        return 'link_url';
    }
    
}