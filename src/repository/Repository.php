<?php
namespace Santosh\Mypackage\Repository;

use  Santosh\Mypackage\Model\DynamicNavbar;
use Santosh\Mypackage\Traits\Eloquent;

class Repository  implements RepositoryInterface
{
    use Eloquent;
    private $model;
    public function __construct(DynamicNavbar $navbar)
    {
        $this->model = $navbar;
    }
    public function saveData(array $data)
    {
        return $this->model->create($data); 
    }
    public function getData()
    {
        try {
            return $this->model->get();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
        

    }
    public function find($id)
    {

    }
    public function findOrFail($id)
    {

    }
    public function update(array $data)
    {
        
    }
    public function getModel()
    {
        return $this->model;
    }
}