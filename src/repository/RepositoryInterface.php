<?php
namespace Santosh\Mypackage\Repository;

interface RepositoryInterface
{
    public function saveData(array $data);
    public function getData();
    public function find($id);
    public function findOrFail($id);
    public function update(array $data);
}