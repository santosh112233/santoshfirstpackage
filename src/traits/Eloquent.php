<?php
namespace Santosh\Mypackage\Traits;

use Illuminate\Database\Eloquent\Model;

trait Eloquent 
{
     /**
     * summary
     */
    protected $data;
    public function __construct(Model $model)
    {
        $this->data = $model;
    }

    public function check()
    {
        return 'checking from traits';
    }

    public function find($id)
    {
    	// dd($id);
    	return $this->model->find($id);
    }
    public function where($array = [])
    {
    	// dd($array);
    	return $this->model->where($array);
    }
    public function with($array = [])
	{
		// dd($array);
	    return $this->model->with($array);
	}

	public function whereHas($attribute, \Closure $closure = null)
	{
		// dd($attribute);
	    return $this->model->whereHas($attribute, $closure);
	}

	public function attach($modal, $method, $data)
	{
		dd($modal,$method);

		return $modal->$method()->attach($data);
	}

    public function paginate($row)
    {
        return $this->model->paginate($row);
    }

}