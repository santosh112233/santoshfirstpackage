<?php
namespace Santosh\Mypackage;

use Santosh\Mypackage\Model\DynamicNavbar;
use Santosh\Mypackage\Repository\RepositoryInterface;
use DB;

class Mypackage
{

    // initializing class

    public $repo;
    public function __construct(RepositoryInterface $interface)
    {
        $this->repo = $interface;
    }
    public static function myPackage()
    {
        return 'this is dynamic navbar package , this is in development mode';
    }

    public   function  getNavar()
    {
    	return DB::table(config('mypackage.database.table'))->get();
    }

    public  function saveData($request)
    {
        return $this->repo->saveData($request); 
    }
    public  function getData()
    {
       return  $this->repo->getData();
   
    }
    public  function getModel()
    {
       return  $this->repo->getModel();
   
    }

}
